// ARITHMETIC OPERATORS
let x = 1397;
let y = 7831;

// Addition Operator
let sum = x + y;
console.log("Result of addition operator: " + sum);

// Subtraction Operator
let difference = x - y;
console.log("Result of subtraction operator: " + difference);

// Multiplication Operator
let product = x * y;
console.log("Result of multiplication operator: " + product);

// Division Operator
let quotient = x / y;
console.log("Result of division operator: " + quotient);

// Modulo Operator
let remainder = y % x;
console.log("Result of modulo operator: " + remainder);

// ASSIGNMENT OPERATOR
// Basic Assignment Operator(=)
// The assignment operator adds the value of the right operand to a variable and assigns the result to the variable
 let assignmentNumber = 8;

// Addition Assignment Operator
// Uses the current value of the variable and it ADDS a number(2) to itself. Afterwards, reassigns it as a new value
// assignmentNumber = assignmentNumber + 2;
assignmentNumber += 2;
console.log("Result of the addition assignment operator: " + assignmentNumber); //10

// Subtraction/ Multiplication/ Division (-=, *=, /=)

// Subtraction Assignment Operator
assignmentNumber -= 2; // assignmentNumber = assignmentNumber - 2;
console.log("Result of the subtraction assignment operator: " + assignmentNumber); //8

// Multiplication Assignment Operator
assignmentNumber *= 2; // assignmentNumber = assignmentNumber * 2;
console.log("Result of the multiplication assignment operator: " + assignmentNumber); //16

// Division Assignment Operator
assignmentNumber /= 2; // assignmentNumber = assignmentNumber / 2;
console.log("Result of the division assignment operator: " + assignmentNumber); //8

// Multiple Operators and Parentheses
/*
	When multiple operators are applied in a single statement, it follows the PEMDAS Rule (Parentheses, Exponents, Multiplication, Division, Addition, and Subtraction)
	1. 3 * 4 = 12
	2. 12 / 5 = 2.4
	3. 1 + 2 = 3
	4. 3 - 2.4 = 0.6
*/

let mdas = 1 + 2 -3 * 4 / 5;
console.log("Result of mdas operation: " + mdas); //0.6000000000000001

let pemdas = 1 + (2 -3) * (4 / 5);
console.log("Result of pemdas operation: " + pemdas); //0.19999999999999996

/*let sample = 5 % 2 * 10;
console.log(sample);*/

// INCREMENT AND DECREMENT
// Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/ decrement was applied to

let z = 1;

// Pre-Increment
let preIncrement = ++z;
console.log("Result of the pre-increment: " + preIncrement); //2
console.log("Result of the pre-increment: " + z); //2

let i = 1;
// Post-Increment
let postIncrement = i++;
console.log("Result of the pre-increment: " + postIncrement); //1
console.log("Result of the pre-increment: " + i); //2

/*
	Pre-increment - adds 1 first before reading value.
	Post-increment - reads value first before adding 1. 

	Post-decrement - subtracts 1 first before reading value.
	Post-decrement - reads value first before subtracting 1. 
*/

let a = 2;
// Pre-Decrement
// The value "a" is decreased by a value of 1 before returning the value and storing it in the variable "preDecrement". 
let preDecrement = --a;
console.log("Result of the pre-decrement: " + preDecrement);
console.log("Result of the pre-decrement: " + a);

let b = 2;
// Post-Decrement
// The value "a" is returned and stored in the variable "postDecrement" the the value of "a" is decreased by 1.
let postDecrement = b--;
console.log("Result of the pre-decrement: " + postDecrement);
console.log("Result of the pre-decrement: " + b);

// TYPE COERCION
/*
	Type coercion is the automatic or implicit conversion of values from one data type or another.
*/

let numA = "10"; //string
let numB = 12; //Number

/*
	Adding/ concatenating a string and a number will result as a string.
*/

let coercion = numA - numB;
console.log(coercion); //1012
console.log(typeof coercion); //String

// Non-coercion
let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

// Addition of Number and Boolean
/*
	The result is a nunber
	The boolean "true" is associated with the value of 1.
	The boolean "false" is associated with the value of 0.
*/

let numE = true + 1;
console.log(numE); //2
console.log(typeof numE); // Number

// COMPARISON OPERATOR

 let juan = "juan";

// Equality Operator (==)
/*
	- Checks wether the operans are equal/ have the same content.
	- Attemps to CONVERT and COMPARE operands of different data type.
	- Returns a boolean value (true / false)
*/

console.log(1 == 1); //True
console.log(1 == 2); //False
console.log(1 == "1"); //True
console.log(1 == true); //True

// compare two strings that are the same
console.log("juan" == "juan"); //true
console.log("true" == true); //false
console.log(true == true); //true
console.log(juan == "juan"); //true
console.log(typeof juan); //string

// Inequality Operator
/*
	- Checks whether the operands are not equal/have different content.
	- Attemps to CONVERT AND COMPARE operands of different data types.
*/
console.log(1 != 1); //false
console.log(1 != 2); //true
console.log(1 != "1"); //false
console.log(1 != true); //false
console.log("juan" != "juan"); //false
console.log("juan" != juan); //false

// Strict Equality Operator (===)
/*
	- Checks whether the operands are equal/ have the same content.
	- It also COMPARES the data types of a two values.
*/
console.log(1 === 1); //true
console.log(1 === 2); //false
console.log(1 === "1"); //false
console.log(1 === true); //false
console.log("juan" === "juan"); //true
console.log(juan === "juan"); //true


// Strict Inequality Operator (!==)
/*
	- Checks whether the operands are not equal/ have the same content.
	- It also COMPARES the data types of a two values.
*/
console.log(1 !== 1); //false
console.log(1 !== 2); //true
console.log(1 !== "1"); //true
console.log(1 !== true); //true
console.log("juan" !== "juan"); //false
console.log(juan !== "juan"); //false

// RELATIONAL OPERATOR
// returns a boolean value.
let j = 50;
let k = 65;

// GT-Greather Than Operator (>)
let isGreaterThan = j > k;
console.log(isGreaterThan); //true

// LT-Less Than Operator (<)
let isLessThan = j < k;
console.log(isLessThan); //false

// GTE-Greater Than or Equal Operator (>=)
let isGTorEqual = j >= k;
console.log(isGTorEqual); //false

// LTE-Less Than or Equal Operator (<=)
let isLTorEqual = j <= k;
console.log(isLTorEqual); //true

let numStr = "30";
// force coercion to change the string to a number.
console.log(j > numStr); //true

let str = "thrity";
// (50 is greater than NaN)
console.log(j > str); //false

// LOGICAL OPERATORS
let isLegalAge = true;
let isRegistered = false;

// Logical AND Operator (&& -Double Ampersand)
// Returns true if all operands are true.
// true && true = true
// true && false = false

let  allRequirementsMet = isLegalAge && isRegistered;
console.log(allRequirementsMet); //false

// Logical OR Operator (|| -Double Pipe)
// Returns true if at least one of the operands is true.
// true || true = true
// true || false = true
// false || false = false

let  someRequirementsMet = isLegalAge || isRegistered;
console.log(someRequirementsMet); //true

// Logical NOT Operator (! - Exclamation point)
// Returns the opposite value
let someRequirementsNotMet = !isRegistered;
console.log(someRequirementsMet);